import React from 'react'
import "./portfolio.css"
import ygoin from '../../assets/ygoin.png'
import viideon from '../../assets/viideon.png'
import vone from '../../assets/vone.png'
import swatch from '../../assets/swatchmasters.png'
import verkehrs from '../../assets/verkehrs-box.png'
import syter from '../../assets/syter5.png'
const data = [
  {
    id: 1,
    image: ygoin,
    title: "YGOIN",
    github: "https://ygoin.com/",
    demo: "https://ygoin.com/"
  },
  {
    id: 2,
    image: viideon,
    title: "Viideon",
    github: "https://app.viideon.com/",
    demo: "https://app.viideon.com/"
  },
  {
    id: 3,
    image: vone,
    title: "VONE",
    github: "https://www.yourvone.com/",
    demo: "https://make.yourvone.com/"
  },
  {
    id: 4,
    image: syter,
    title: "Syter",
    github: "https://m.facebook.com/syter.france.96",
    demo: "http://syter.fr/"
  },
  {
    id: 5,
    image: swatch,
    title: "SwatchMasters",
    github: "http://swatchmasters.com/",
    demo: "http://swatchmasters.com/"
  },
  {
    id: 6,
    image: verkehrs,
    title: "Verkehrs Box",
    github: "https://verkehrsbox.com/",
    demo: "http://github.com"
  }
]
const Portfolio = () => {
  return (
    <section id='portfolio'>
      <h5> My Recent Work </h5>
      <h2> Portfolio </h2>

      <div className='container portfolio__container'>
        {data.map(item => (
          <article className='portfolio__item' key={item.id}>
            <div className='portfolio__item-image'>
              <img src={item.image} alt={item.title} />
            </div>

            <h3>{item.title}</h3>
            <div className='portfolio__item-cta'>
              <a href={item.github} className='btn' target="_blank" rel="noreferrer">GitHub</a>
              <a href={item.demo} className='btn btn-primary' target="_blank" rel="noreferrer">Live Demo</a>
            </div>
          </article>
        ))}

      </div>
    </section>
  )
}

export default Portfolio