import React, { useState } from 'react'
import "./nav.css"
import {AiOutlineHome} from 'react-icons/ai';
import {AiOutlineUser} from 'react-icons/ai';
import {BiBook} from 'react-icons/bi';
import {BiMessageSquareDetail} from 'react-icons/bi';
import {RiServiceLine} from 'react-icons/ri';
const Nav = () => {
  const [activeTab, setActiveTab] = useState('#')
  return (
    <nav>
      <a href='/#' className={activeTab === '#'?'active' : ''} onClick={() => setActiveTab('#')} rel="noreferrer" > <AiOutlineHome /> </a>
      <a href='/#about' className={activeTab === '#about'?'active' : ''} onClick={() => setActiveTab('#about')} rel="noreferrer" > <AiOutlineUser /> </a>
      <a href='/#experience' className={activeTab === '#experience'?'active' : ''} onClick={() => setActiveTab('#experience')} rel="noreferrer" > <BiBook /> </a>
      <a href='/#services' className={activeTab === '#services'?'active' : ''} onClick={() => setActiveTab('#services')} rel="noreferrer" > <RiServiceLine /> </a>
      <a href='/#contact' className={activeTab === '#contact'?'active' : ''} onClick={() => setActiveTab('#contact')} rel="noreferrer" > <BiMessageSquareDetail /> </a>
    </nav>
  )
}

export default Nav