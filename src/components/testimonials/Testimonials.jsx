import React from "react";
import "./testimonials.css";

// import Swiper core and required modules
import { Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

import khalid from '../../assets/khalidfakharany.webp'
import abdulbasit from '../../assets/Abdulbasit.png'
import thomasswaney from '../../assets/ThomasSwaney.png'
import markus from '../../assets/Markus.png'
const data = [
  {
    id: 1,
    avatar: thomasswaney,
    name: "Thomas Swaney",
    review:
    "I’m so grateful that we are celebrating 1 year of having you as a Software Engineer. We're thrilled to work with you every day. Yes, you’re the best SE ever, and you make my job a breeze.",
  },
  {
    id: 2,
    avatar: khalid,
    name: "khalid Fakharany",
    review:
    "It was a pleasure working with you. You did a good job :clap: I hope all the best for you. Thanks Hamza",
  },
  {
    id: 3,
    avatar: markus,
    name: "Markus",
    review:
    "Muhammad! You are great in managing the project and I love how you handle complex task in a very decent ways. You are such a friendly person. I say good luck for the future",
  },
  {
    id: 4,
    avatar: abdulbasit,
    name: "Abdul Basit",
    review:
      "Hamza! You have extra ordinary skills. I love the way you handle hashmeeting.",
  },
];
const Testimonials = () => {
  return (
    <section id="testimonials">
      <h5> Review from clients </h5>
      <h2> Testimonials </h2>

      <Swiper
        className="container testimonials__container"
        modules={[ Pagination]}
        spaceBetween={40}
        slidesPerView={1}
        pagination={{ clickable: true }}
      >
        {data.map(({ id, avatar, name, review }) => (
          <SwiperSlide className="testimonial" key={id}>
            <div className="client__avatar">
              <img src={avatar} alt={name} />
            </div>

            <h5 className="client__name">{name}</h5>
            <small className="client__review"> {review} </small>
          </SwiperSlide>
        ))}
      </Swiper>
    </section>
  );
};

export default Testimonials;
