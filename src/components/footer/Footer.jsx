import React from 'react'
import "./footer.css"
import {FaLinkedinIn} from 'react-icons/fa'
import {FiInstagram} from 'react-icons/fi'
import {IoLogoTwitter} from 'react-icons/io'
const Footer = () => {
  return (
    <footer>
      <a href='/#' className='footer__logo' rel="noreferrer" >H2O </a>
      <ul className='parmalinks'>
        <li><a href='/#' rel="noreferrer" >Home</a></li>
        <li><a href='/#about' rel="noreferrer" >About</a></li>
        <li><a href='/#experience' rel="noreferrer" >Experience</a></li>
        <li><a href='/#services' rel="noreferrer" >Services</a></li>
        <li><a href='/#portfolio' rel="noreferrer" >Portfolio</a></li>
        <li><a href='/#testimonials' rel="noreferrer" >Testimonials</a></li>
        <li><a href='/#contact' rel="noreferrer" >Contact</a></li>
      </ul>

      <div className='footer__socials'>
        <a href='https://www.linkedin.com/in/muhammadhamza97/' target="_blank" rel="noreferrer" > <FaLinkedinIn /> </a>
        <a href='https://www.instagram.com/muhammadhamza.97/' target="_blank" rel="noreferrer" > <FiInstagram /> </a>
        <a href='https://twitter.com/MHamzaTauqeer' target="_blank" rel="noreferrer" > <IoLogoTwitter  /> </a>
      </div>

      <div className='footer__copywright'>
        <small>&copy; H2O All rights reserved.</small>
      </div>
    </footer>
  )
}

export default Footer