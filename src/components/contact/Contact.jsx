import React, { useRef } from "react";
import "./contact.css";
import { MdOutlineEmail } from "react-icons/md";
import { RiMessengerLine } from "react-icons/ri";
import { BsWhatsapp } from "react-icons/bs";
import emailjs from "@emailjs/browser";
const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();
    if(!form.current.email.value) return alert('Email is requried!')
    if(!form.current.name.value) return alert('Name is requried!')
    form.current.subject.value = form.current.name.value
    emailjs
      .sendForm(
        "service_ijs6h4s",
        "template_nkl010h",
        form.current,
        "xPiE66E4Z1jfpcDjN"
      )
      .then(
        (result) => {
        },
        (error) => {
          console.log(error.text);
        }
      );
    e.target.reset();
  };

  return (
    <section id="contact">
      <h5> Get In Touch </h5>
      <h2> Contact Me </h2>

      <div className="container contact__container">
        <div className="contact__options">
          <article className="contact__option">
            <MdOutlineEmail className="contact__option-icon" />
            <h4>Email</h4>
            <h5 className="text-light">hamza.dev1997@gmail.com</h5>
            <a
              href="mailto:hamza.dev1997@gmail.com"
              target="_blank"
              rel="noreferrer"
            >
              Email
            </a>
          </article>
          <article className="contact__option">
            <RiMessengerLine className="contact__option-icon" />
            <h4>Massenger</h4>
            <h5 className="text-light">Muhammad Hamza</h5>
            <a
              href="https://m.me/MuhammadHamza1997"
              target="_blank"
              rel="noreferrer"
            >
              Message
            </a>
          </article>
          <article className="contact__option">
            <BsWhatsapp className="contact__option-icon" />
            <h4>WhatsApp</h4>
            <h5 className="text-light">+92-314-4477-352</h5>
            <a
              href="https://api.whatsapp.com/send?phone=+923144477352"
              target="_blank"
              rel="noreferrer"
            >
              Chat
            </a>
          </article>
        </div>
        {/* END of OPTIONS */}
        <form ref={form} action="" onSubmit={sendEmail}>
          <input
            type="text"
            name="name"
            placeholder="Your Full Name"
            required
          ></input>
          <input
            type="email"
            name="email"
            placeholder="Your Email"
            required
          ></input>
          <textarea
            name="message"
            rows={7}
            placeholder="Your Message"
            required
          ></textarea>
          <button type="submit" className="btn btn-primary">
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
};

export default Contact;
