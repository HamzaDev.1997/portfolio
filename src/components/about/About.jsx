import React from "react";
import "./about.css";
import { FaAward } from "react-icons/fa";
import { FiUsers } from "react-icons/fi";
import { VscFolderLibrary } from "react-icons/vsc";
import ME from "../../assets/ABOUT_ME.png";
const About = () => {
  return (
    <section id="about">
      <h5> Get to know </h5>
      <h2> About Me </h2>

      <div className="container about__container">
        <div className="about__me">
          <div className="about__me-image">
            <img src={ME} alt="About " />
          </div>
        </div>

        <div className="about__content">
          <div className="about__cards">
            <article className="about__card">
              <FaAward className="about__icon" />
              <h5>Experience</h5>
              <small>04+ years working</small>
            </article>
            <article className="about__card">
              <FiUsers className="about__icon" />
              <h5>Clients</h5>
              <small>08+ woldwide</small>
            </article>
            <article className="about__card">
              <VscFolderLibrary className="about__icon" />
              <h5>Projects</h5>
              <small>012+ completed </small>
            </article>
          </div>

          <p>
            I am a full-stack software engineer and Mobile app developer having
            more than four(4) years of experience. I have developed multiple
            applications with thousands of live users. From small to large
            scall/scop applications and Mobile application to PWA I have all the
            experties in hand. YGOIN, SYTER, Viideon, Hashmeeting, travus, and
            many more to count under my projects.
          </p>

          <a href="#contact" className="btn btn-primary">
            Let's Talk
          </a>
        </div>
      </div>
    </section>
  );
};

export default About;
