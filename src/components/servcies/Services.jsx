import React from 'react'
import "./services.css"
import {BiCheck} from 'react-icons/bi'
const Services = () => {
  return (
    <section id='services'>
      <h5> What I Offer </h5>
      <h2> Services </h2>

      <div className='container services__container'>
        <article className='service'>
          <div className='service__head'>
            <h3>Mobile App Development</h3>
          </div>
          <ul className='service__list'>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I will design your application from scratch in React Native Android. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I will design your application from scratch in React Native iOS. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can also give support for your Mobile Application. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I will design your iOS application from scratch. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can also give support for your iOS Application. </p>
            </li>
          </ul>
        </article>
        {/* END OF UI/UX  */}
        <article className='service'>
          <div className='service__head'>
            <h3>Web Development</h3>
          </div>
          <ul className='service__list'>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can build you a complete responsive web application according to your business needs. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can build Progressive Web Application(PWA) for your business. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can build pixel perfect one page website or desing you single page resume. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can give you support for your already live project and add new featurs to it. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can provide complete support for you business application. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I have some bonus modules for you as an adds on. </p>
            </li>
          </ul>
        </article>
        {/* END OF WEBDEVELOPMENT */}
        <article className='service'>
          <div className='service__head'>
            <h3>DevOps</h3>
          </div>
          <ul className='service__list'>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can automate your application deployment so you can relax of deploying your application each time. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can deploy your application to any cloud (AMAZON, GC, Digital Ocean). </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can help to find out cost effective solutions according to your business needs. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can optimise your application. </p>
            </li>
            <li>
              <BiCheck className='service__list-icon' />
              <p> I can secure your application with best practices. </p>
            </li>
          </ul>
        </article>
        {/* END OF CONTENT CREATION */}

      </div>
    </section>
  )
}

export default Services